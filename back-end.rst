========
Back-end
========

Storage and Deployment
======================

.. index:: CondDB storage

LHCb Conditions Database is stored in a Git repository hosted on the CERN Gitlab
instance, at https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database.


.. index:: CondDB deployment

Although the repository is publicly accessible and can be cloned locally with
just a ``git clone``, we keep a (bare) mirror of it on CVMFS, at
``/cvmfs/lhcb-condb.cern.ch/git-conddb/lhcb-conditions-database.git``, so that
it is immediately accessible to jobs running on the Grid or on the Event Filter
Farm as the software libraries.


.. _Git CondDB:

The *Git CondDB* Library
========================

.. index:: Git CondDB library

To access the condition data inside Git repositories we developed an
experiment agnostic library, available under Apache-2 license at
http://gitlab.cern.ch/lhcb/GitCondDB.

The main goal of this access layer is to map the 2-dimensional Git repository
internal database (with *version* and *filename* axes), to the 3-dimensional
space of a Conditions Database (with *version*, *source* and
*time* axes).


.. _Git CondDB layout:

Layout
------

While for tracking a condition version we directly use the version concept of
the Git repository, the condition source is conventionally mapped to the
filename of an object in the repository, which allows us to organize conditions
in groups identified by folders in the repository.

The IOVs concept does not have a natural mapping in the Git repository database 
(or in a filesystem), so we use a special convention to attach multiple values,
one per IOV, to a condition source: we use files inside a directory, named after
the IOV their content is valid for.

To better understand the technique, let's consider the case of the lookup for a
specific condition, for a *version*, *filename* and *time* triplet (note that in
this context *time* is just an integer number used to identify a specific moment
in time, with no constraint on the units used).

#. we limit the lookup to the state of the repository at the requested version
#. look for the object identified by the filename
#. if it is:

   a regular file
      we use the content of the file as condition value, regardless of the
      requested time (infinite or unbounded IOV)

   a directory [#fn-dir-flag]_
      the filenames inside it must be numbers stating the start of the IOV
      their content is valid for, so we look for the *highest* filename that is
      less or equal to the requested time, and we use the content of that file
      of condition value, setting the IOV for the condition to start at the time
      corresponding to the filename value, and to end at the value of the next
      filename, or infinity if we hit the last file

To speed up the lookup in case of many IOVs in a condition, the IOV lookup
algorithm is recursive, so instead of a file, one can nest directories to group
IOVs, allowing for a logarithmic lookup complexity, instead of a linear one.


.. rubric:: Footnotes

.. [#fn-dir-flag] For historical reasons, the directory must contain the special file
   ``.condition`` (the content is not used, so it can be empty). 
