.. _use-cases:

====================
Conditions Use Cases
====================

Conditions values in the Conditions Database are used in different contexts and
have to support different use cases, in particular simulation and real data
processing.

Simulation and real data processing use of conditions differ because while for
real data we are interested in the time evolution of conditions, for simulation
we need to be able to explore the response of the detector for fixed sets of
conditions. To better accommodate the two use cases we track condition data
differently depending on the task.

For real data we record the time axis of the conditions database to follow the
evolution of the conditions over time, and we use Git versioning of data to
track the evolution of our understanding of the detector (e.g. better
understanding of the repsonse of a subdetector, or simply improved alignment
algorithms). Sometimes we have to apply changes to the conditions database
content because of the evolution of our software, in which case we use different
Git branches to isolate the versions of the conditions compatible with one or
another version of the software.

In the simulation use case we do not need to track the time evolution of the
condition value, as the simulated run number is not meant to represent a time
evolution of the simulated data, but it is used as a mean to control the
reproducibility of the simulated data (as it participates in the random number
seed). What we do instead is to maintain a series of Git branches to follow the
compatibility of the data with the code.


.. index:: Online conditions, Offline conditions

Also in the context of real data processing we have to distinguish between two
different use cases: *offline* and *online* conditions. We call *online
conditions* those conditions that are generated automatically (without human
intervention) like alignments from automated alignment jobs or temperatures and
pressures retrieved from probes. *Offline conditions*, instead, are the
conditions that are produced manually like special calibrations that require
some expert to run analysis jobs and validate the results before requesting
inclusion in the database. The main difference between these use cases is that
*online conditions* are automatically pushed to the Gitlab project and published
to CVMFS, while the *offline conditions* have to be proposed in form of Gitlab
merge requests, to undergo a review  before being integrated and published. In
both cases changes are integrated in the main branch of the Git repository
(``main``), but a special branch (``offline``) is used when we need to override
some of the condition values produced automatically (see :ref:`override
online`). More details on the procedure are in the section
:ref:`update-procedures`.