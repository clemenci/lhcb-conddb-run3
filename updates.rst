.. _update-procedures:

=================
Update Procedures
=================

Here we describe in some detail how condition values get published to the
Conditions Database Git repository.

.. _update online:

Online
------

.. index:: Online conditions

As described in :ref:`use-cases`, *online conditions* are automatically
generated and get automatically uploaded to Gitlab and deployed to CVMFS.

Two main type of online conditions are identified:

- conditions not bound the runs (like readings from sensors and probes connected
  to the WinCC-OA Service)
- conditions bound to specific runs (like those produced by
  alignment and calibration tasks)

In both cases, some time before the start of a run, the new condition values are stored in
a dedicated location on a filesytem shared with all nodes in the online
computing farm. The name of the new files (as described in the :ref:`Git CondDB
layout section <Git CondDB layout>`) have to be the new run number (the various
files can be grouped in directories named after the first run they are hosting,
for example rounding by hundreds, ``int(run_number / 100) * 100``, or by
multiples of 2, ``run_number & ~0x3f``). If the values did not change with
respect to the previous run, we can add a symlink instead of a regular file.

Once all the files for the run are saved, they are committed to the Git
repository and the new set of values puhed to Gitlab. It must be noted that it
is not mandatory to commit or push every run, as the Hlt jobs use the files and
not the repository database, but it is important to commit and push often, to
avoid the risk of loss of data.


Conditions Not Bound to Specific Runs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In some cases we monitor automatically the evolution of some condition and when
the value changes by more than a given threashold we store it in the database
using as file name the current run number plus one, so that the new value is
picked up for the next runs.


Conditions Bound to Specific Runs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Some conditions require synchronization with the Hlt processes and to achieve it
we must have one file per run number even if there are no changes with respect
to the previous run. The rationale for this policy is that if find the file with
the name we expect (current run number) we can be sure that the data has been
added in time, but if the file is missing we cannot be sure that it is because
of no change or because it did not appear yet due to some hick up in the
filesystem.


.. _update offline:

Offline
-------

.. index:: Offline conditions

Conditions that do not require a prompt update to be used in Hlt jobs
(conditions for simulation fall in this category) or that cannot be produced
automatically (for example because they require human validation) are integrated
in the Conditions Database Git repository through a standard Git workflow:

#. the developer creates a new branch from the branch she wants to update
#. apply the required changes (edit, commit, push cycle)
#. she creates a *merge request* to ask for the changes to be integrated
#. a CondDB maintainer review the merge request and if OK merges the changes

If the changes are small, one can use the `Gitlab integrated Web IDE
<https://gitlab.cern.ch/-/ide/project/lhcb-conddb/lhcb-conditions-database/edit/master/-/>`_,
which provides a simplified interface implementing the described workflow.

Once the changes are merged, an automated task will pull them to the online
shared filesystem, so that they are accessible to the Hlt farm (if applicable).

To be noted that production jobs on the Grid require a Git tag to access
condition values, so that we can guarantee the reproducibility of the results.
