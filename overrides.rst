=========
Overrides
=========

Sometimes the content of the Conditions Database requires changes to some of the
values, permanent or transient. In any case we must ensure consistency and
reproducibility.

.. _override online:

Overrides of Online Conditions
------------------------------

In the case of Online conditions, an *override* is usually a permanent change of
a condition value that at some point in time was used by the Hlt processes. By
construction the Online Hlt processes never access conditions of old runs, so
when we need an override it is only to be used by Offline jobs.

To override conditions values for Offline processing but preserve the values
used Online we work with two parallel branches: ``main`` and ``offline``.

The branch ``main`` is automatically updated from Online automated tasks and via
merge requests on Gitlab (see :ref:`update-procedures`), but merge requests
should never modify the files updated by Online tasks.  The branch ``offline``
receives regular automatic merges from ``main`` and can be targeted by merge
requests for Online specific entries. Tags used for offline processing must only
be created on the ``offline`` branch (unless required for special productions).

This policy makes it possible to present two *views* of the conditions, one for
the benefit of the Online system guaranteeing that the values seen there are
those used for the Online processing of events, and a view for offline
productions that can benefit from fixes to the conditions values (imagine a
probe not correctly calibrated or a mistake in cabling mapping).

We do not explicitly consider the possibility of transient override of Online
conditions because they are usually meant for cases where permanent changes,
which should always be preferred, are not applicable or pratical, like in some
Simulation cases.

.. _override simulation:

Overrides for Simulation
------------------------
