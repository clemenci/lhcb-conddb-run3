============================
Intervals Of Validity (IOVs)
============================

The :ref:`Git CondDB library <git conddb>` uses unsigned integers to
express time, but it doesn't put any constraint on the meaning of those numbers,
so it is responsibility of the user to choose the units of time to use.

LHCb uses *run numbers* as time unit in the conditions database. A run number is a
monotonically increasing number associated to a small period of data taking
[#fn-run-number]_ (maximum one hour).

Although run numbers do not directly identify specific moments in time, there
are no ambiguities by construction. Moreover it is a consolidated convention in
the experiment to identify specific periods of data taking by the range of run
numbers used in that period (e.g. "the Pb-Pb collisions are recorded in runs
1234 to 1300"), also because they are used as keys for the lookup of data files
in the LHCb Bookkeeping Database.

One particular advantage of run numbers over a more traditional time unit (like
seconds) is that run numbers implicitly carry the information of the end of the
run they refer, i.e. we know exactly at which wall clock time a run starts, but
we do not know when it will end, but we know that the following run will have a
different run number. This feature is extremely useful when we need to
synchronize updates of conditions with the data taking, like in the HLT
applications. In that case we can enforce that the HLT processes check if new
condition values are available for the events they are processing every time
they detect a change of run number.

The only draw back of the use of run numbers with respect to other time units is
that the interval boundaries are synchronized with the data taking. So if a
condition (like air pressure) changes substantially within a run, that
information is not accurately recorded in the conditions database. Luckily there
are not many conditions that require to be followed that closely, but if the
case occurs, we can stop a run before the one hour limit [#fn-one-hour-runs]_
and start a new run to pick up the new condition value.


.. rubric:: Footnotes

.. [#fn-run-number] Different run numbers are also used to identify special test
   runs and are guaranteed not to clash with the numbers used for data taking.
.. [#fn-one-hour-runs] It must be noted that the maximum duration of a run has
   been chosen to be approximately one hour as we note that data taking
   conditions are usually stable within the hour.