============
Introduction
============

LHCb, as any other High Energy Experiment, makes use of a Conditions Database to keep
track of the time dependent, non-event data required to process the data
collected by the experiment detector.

Since 2015 LHCb uses a Conditions Database solution leveraging on a Git
repository as storage backend, known as Git CondDB.

For the LHC Run 3 data taking, LHCb reorganized it's use of the Conditions
Database based on the experience gathered during LHC Run 2 data taking.

Conditions Database
===================

.. index:: Conditions Database

A *Conditions Database* is a database used to store *time varying, non-event data*
required for the reconstruction and analysis of physics events collected by the
experiment.

Conditions typically live in a 3-dimensional space, with *version*, *source* and
*time* as axes.

.. image:: images/CondDBBlocks.png

.. index:: Intervals Of Validity
   see: IOVs; Intervals Of Validity

Since a condition value is usually valid for multiple events (unlike event
data, that is, by definition, valid only for the specific event), we often refer
to *Intervals Of Validity*, or *IOVs*, as the period of time a specific
condition value is valid for.

Although the axes are in principle independent, they have in practice a
hierarchy, and we always refer to the version as a complete set of values for
all the sources, and a source (or condition) is always used to refer to all the
time dependent values it has.

Several technologies have been and are used by various experiments to implement the Conditions Database
concept.  LHCb used the CERN-IT developed library `COOL
<https://twiki.cern.ch/twiki/bin/view/Persistency/Cool>`_ for Run1 data taking,
until the new :ref:`Git-based solution <chep2018>` was developed and commissioned.
