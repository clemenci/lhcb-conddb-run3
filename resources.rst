=========
Resources
=========

.. _chep2018:

- CHEP2018: A Git-based Conditions Database backend for LHCb

  - `slides <https://indico.cern.ch/event/587955/contributions/2936870/attachments/1682732/2703988/A_Git_based_Conditions_Database_backend_for_LHCb__CHEP_2018_.pdf>`__
    (`overleaf <https://www.overleaf.com/project/5b2770d326f0944255ad7898>`__)
  - `proceedings <https://doi.org/10.1051/epjconf/201921404037>`__ (`overleaf <https://www.overleaf.com/project/5bbc865a60e1ea4f754e5e34>`__)


.. _12th-computing-workshop:

- 12th LHCb Computing Workshop

  - `Status of Conditions Database <https://indico.cern.ch/event/831054/contributions/3645815/>`_