.. _format:

============================
Conditions Persistent Format
============================

We decided to use `YAML`_ to describe condition data on disk, because it's a
good compromise between human readability and efficiency (less verbose than XML
and more powerful than JSON), and it is quite common (for example in Gilab CI/CD
configuration files).

To avoid tight coupling between the LHCb Physics Software and the conditions
persistent format, we are using only standard YAML features and internally we
try to stick as much as possible to the regular data structures provided by the
C++ YAML implementation we use.

Examples of condition files could be something like

.. code-block:: yaml
    :caption: MyDetector/alignment.yaml

    # Alignments for my modules
    MyModule1: !alignment
        position: [0.0, 0.0, 0.1]
        rotation: [0.0, 0.0, 0.0]

or

.. code-block:: yaml
    :caption: MyDetector/readout.yaml

    cabling-map:
      # map of connections as
      # panel -> position -> module
      1:
        1: 5
        2: 4
        3: 1
      2:
        1: 2
        2: 6
        3: 3


Special tags like ``!alignment`` can be used to map data to special C++ classes
and for automatic validation of the content, but could be ignored in generic
parsing tools.


.. _YAML: https://yaml.org